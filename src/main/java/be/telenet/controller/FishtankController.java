package be.telenet.controller;

import be.telenet.service.AquariumService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FishtankController {

    private Log log = LogFactory.getLog(FishtankController.class);

    @Autowired
    private AquariumService aquariumService;

    @GetMapping("/")
    public String fishtank(Model model) {
        model.addAttribute("lightState", aquariumService.getLightState());

        return "fishtank";
    }

    @GetMapping("/setlightstrength")
    public String setlightstrength(@RequestParam("val") Integer lightStrength, Model model) {

        try {
            aquariumService.setLightStrength(lightStrength);
        } catch (Exception e) {
            log.error("setting lights failed.",e);
        }

        model.addAttribute("lightState", aquariumService.getLightState());
        return "fishtank";
    }

    @GetMapping("/setfromtime")
    public String setFromTime(@RequestParam("val") String fromTime, Model model) {

        try {
            aquariumService.setFromTime(fromTime);
        } catch (Exception e) {
            log.error("setting lights failed.",e);
        }

        model.addAttribute("lightState", aquariumService.getLightState());
        return "fishtank";
    }

    @GetMapping("/settilltime")
    public String setTillTime(@RequestParam("val") String tillTime, Model model) {

        try {
            aquariumService.setTillTime(tillTime);
        } catch (Exception e) {
            log.error("setting lights failed.",e);
        }

        model.addAttribute("lightState", aquariumService.getLightState());
        return "fishtank";
    }


}

