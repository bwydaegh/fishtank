package be.telenet.domain;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public class LightState {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("HH:mm");
    private boolean on;
    private int strength = 3000;
    private String onFrom = "0:07";
    private String onTill = "20:00";


    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getOnFrom() {
        return onFrom;
    }

    public void setOnFrom(String onFrom) {
        this.onFrom = onFrom;
    }

    public String getOnTill() {
        return onTill;
    }

    public void setOnTill(String onTill) {
        this.onTill = onTill;
    }

    public boolean resolveOn(Date currentTime) {
        sanitize();
        LocalTime fromDate = LocalTime.parse(onFrom);
        LocalTime tillDate = LocalTime.parse(onTill);
        LocalTime time = LocalDateTime.ofInstant(currentTime.toInstant(), ZoneId.systemDefault()).toLocalTime();
        on = time.isAfter(fromDate) && time.isBefore(tillDate);
        return on;
    }

    private void sanitize() {
        int i = onFrom.indexOf(":");
        if (i == 1) {
            onFrom = "0" + onFrom;
        }
        i = onTill.indexOf(":");
        if (i == 1) {
            onTill = "0" + onTill;
        }
    }


    public static void main(String[] args) {
        LightState ls = new LightState();
        System.out.println(ls.resolveOn(new Date()));
    }
}