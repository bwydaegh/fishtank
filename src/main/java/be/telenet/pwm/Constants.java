package be.telenet.pwm;

import com.pi4j.io.i2c.I2CBus;

public class Constants {

    /**
     * PWM Control
     */

    public static final int[] IC_ADDRESSES = {0x40,0x48,0x70};

    public static final int I2C_BUS = I2CBus.BUS_3;

    public static final int MODE_1 = 0;
    public static final int MODE_2 = 1;

    public static final int PRE_SCALER = 254;

    public static final float OSC_CLOCK = 25000000.0f;

    public static final int POWER_SUPPLY_ADDRESS = 15;

    public static final int MAX_PWM_VALUE = 4095;


    /** pwmFrequency boundaries */

    public static final int PWM_MIN_FREQUENCY = 48;
    public static final int PWM_MAX_FREQUENCY = 1500;


}
