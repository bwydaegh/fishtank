package be.telenet.pwm;


import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;


/**
 * The main class of the PWM control, connects to the pwm board
 */
@Component
public class PWMCore {
    private static final Log log = LogFactory.getLog(PWMCore.class);

    private I2CDevice device;

    private boolean isConnected;

    public I2CDevice getDevice() {
        return device;
    }

    public void connect() {
        log.info("Connecting to the I2C Bus ...");

        try {
            initI2C();
            this.isConnected = true;
        } catch (Exception e) {
            log.error("Can't connect to the pwm core", e);
            this.isConnected = false;
        }
    }

    public void setPWMFrequency(int frequency) throws IOException {
        int oldMode = getDevice().read(Constants.MODE_1);
        int newMode = (oldMode & 0x7F) | 0x10;
        getDevice().write(Constants.MODE_1, (byte) newMode);
        getDevice().write(Constants.PRE_SCALER, (byte) calculatePWMFrequency(frequency));
        getDevice().write(Constants.MODE_1, (byte) oldMode);
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getDevice().write(Constants.MODE_1, (byte) (oldMode | 0x80));


    }

    public void setPWMStrength(int ledIndex, int lightStrength) throws IOException {

        log.info("Set light strenght of led index " + ledIndex + " to " + lightStrength);

        if (lightStrength < 0 || lightStrength > 4095) {
            log.error("Error: light strength is " + lightStrength + ". It needs to be in range 0 - 4095");
            exit();
        }
        byte REG_ON_L = (byte) (ledIndex * 4 + 6);
        byte REG_ON_H = (byte) (ledIndex * 4 + 6 + 1);
        byte REG_OFF_L = (byte) (ledIndex * 4 + 6 + 2);
        byte REG_OFF_H = (byte) (ledIndex * 4 + 6 + 3);

        byte ON_L = 0;
        byte ON_H = 0;

        byte OFF_L = (byte) ((float) lightStrength % 256);
        byte OFF_H = (byte) ((float) lightStrength / 256);

        getDevice().write(REG_ON_L, ON_L);
        getDevice().write(REG_ON_H, ON_H);
        getDevice().write(REG_OFF_L, OFF_L);
        getDevice().write(REG_OFF_H, OFF_H);

    }

    public void turnOnPowerSupply(boolean on) throws IOException {

        if (on) {
            this.setPWMStrength(Constants.POWER_SUPPLY_ADDRESS, 0);
        } else {
            log.info("Shutting down the power supply ...");
            this.setPWMStrength(Constants.POWER_SUPPLY_ADDRESS, 4095);
        }
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    private void initI2C() throws Exception {

        for (int adr : Constants.IC_ADDRESSES) {
            log.debug("getting the I2C device on the right bus and address...addr:" + adr);
            try {
                device = I2CFactory.getInstance(Constants.I2C_BUS).getDevice(adr);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Initialize the pwmSettings
            log.debug("Writing initial measurementSettings to the IC");
            try {
                device.write(Constants.MODE_1, (byte) 175);
                device.write(Constants.MODE_2, (byte) 4);
                log.info("Successfully initialised the I2C!");
                break;
            } catch (IOException e) {
                log.error("Error: An IOException occurred. Probably the i2cdevice wasn't found.");
            }
        }
    }

    private int calculatePWMFrequency(int pwmFrequency) {
        if (pwmFrequency < Constants.PWM_MIN_FREQUENCY || pwmFrequency > Constants.PWM_MAX_FREQUENCY) {
            log.error("Error: Frequency = " + pwmFrequency + ". This is not allowed");
            exit();
        }

        float scaleValue = Constants.OSC_CLOCK / (Constants.MAX_PWM_VALUE + 1) / (float) pwmFrequency - 1;
        double preScale = Math.floor(scaleValue + 0.5f);

        return (int) preScale;
    }


    private void exit() {
        log.error("Exiting program...");
        System.exit(-1);
    }


}
