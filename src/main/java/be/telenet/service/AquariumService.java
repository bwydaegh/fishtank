package be.telenet.service;

import be.telenet.domain.LightState;

import java.io.IOException;

public interface AquariumService {

    LightState getLightState();

    void setLightStrength(int lightStrength) throws IOException;

    void setFromTime(String fromTime) throws IOException;

    void setTillTime(String tillTime) throws IOException;
}
