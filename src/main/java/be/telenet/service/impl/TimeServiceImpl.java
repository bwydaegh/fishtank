package be.telenet.service.impl;

import be.telenet.service.TimeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Date;

@Service
public class TimeServiceImpl implements TimeService, InitializingBean {
    private Log log = LogFactory.getLog(AquariumServiceImpl.class);

    @Value("${time.server.urls}")
    private String[] timeServerUrls;

    private NTPUDPClient timeClient;
    private InetAddress timeServerAddress;

    @Override
    public void afterPropertiesSet() throws Exception {
        timeClient = new NTPUDPClient();
        timeClient.setDefaultTimeout(2000);
        initTimeServerClient();
    }

    private void initTimeServerClient(){
        for (String timeServerUrl : timeServerUrls) {
            try {
                timeServerAddress = InetAddress.getByName(timeServerUrl);
                timeClient.getTime(timeServerAddress);
                log.info("Using time server url:"+timeServerUrl);
                break;
            } catch(SocketTimeoutException socketTimeoutException){
                log.warn("Time server url:"+timeServerUrl + " timed out.");
            } catch (Exception e) {
                log.warn("Time server url:"+timeServerUrl + ". Encountered unexpected exception.");
            }
        }
    }

    @Override
    public synchronized Date getCurrentTime() {

        try {
            TimeInfo timeInfo = timeClient.getTime(timeServerAddress);
            long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
            log.trace("Current time is:" + new Date(returnTime));
            return new Date(returnTime);
        } catch(SocketTimeoutException socketTimeoutException){
            log.warn("Time server timed out. Re-initialising time client and returning system time:" + new Date());
            initTimeServerClient();
            return new Date();
        } catch (Exception e) {
            log.warn("Could not fetch time. Returning system time:" + new Date(), e);
            return new Date();
        }

    }
}
