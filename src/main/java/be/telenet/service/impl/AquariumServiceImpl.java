package be.telenet.service.impl;

import be.telenet.domain.LightState;
import be.telenet.pwm.PWMCore;
import be.telenet.service.AquariumService;
import be.telenet.service.TimeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

@Service
public class AquariumServiceImpl implements AquariumService {
    private Log log = LogFactory.getLog(AquariumServiceImpl.class);

    @Autowired
    private PWMCore pwmCore;

    @Autowired
    private TimeService timeService;

    private LightState lightState = new LightState();

    @Scheduled(fixedDelay = 5000)
    public void scheduleLightCheck() {
        doLightCheck();
    }

    private synchronized void doLightCheck() {
        try {
            log.trace("evaluating lights. was:" + lightState.isOn());
            Date currentTime = timeService.getCurrentTime();
            boolean existingState = lightState.isOn();
            boolean newState = lightState.resolveOn(currentTime);
            if (existingState != newState) {
                log.info("Switching lights from:" + existingState + " to:"+newState);
                if (newState) {
                    setLightStrength(4000);
                } else {
                    setLightStrength(0);
                }
            }
            log.trace("evaluating lights. and is now:" + lightState.isOn());
        } catch (Exception e) {
            log.error("Light check error.", e);
        }
    }

    public synchronized void setLightStrength(int lightStrength) throws IOException {
        log.trace("setLightStrength:" + lightStrength);
        if (!pwmCore.isConnected()) {
            pwmCore.connect();
        }

        pwmCore.setPWMStrength(0, lightStrength);
        pwmCore.setPWMStrength(1, lightStrength);
        pwmCore.setPWMStrength(2, lightStrength);
        pwmCore.setPWMStrength(3, lightStrength);
        pwmCore.setPWMStrength(4, lightStrength);
        pwmCore.setPWMStrength(5, lightStrength);
        pwmCore.setPWMStrength(6, lightStrength);

        lightState.setStrength(lightStrength);
        log.trace("Light strength is now:" + lightStrength);
    }

    @Override
    public void setFromTime(String fromTime)  {
        log.trace("setFromTime:" + fromTime);
        lightState.setOnFrom(fromTime);
        doLightCheck();
    }

    @Override
    public void setTillTime(String tillTime) {
        log.trace("setTillTime:" + tillTime);
        lightState.setOnTill(tillTime);
        doLightCheck();
    }

    public LightState getLightState() {
        return lightState;
    }

    public void setLightState(LightState lightState) {
        this.lightState = lightState;
    }


}
