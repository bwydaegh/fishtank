package be.telenet.service;

import java.util.Date;

public interface TimeService {
    Date getCurrentTime();
}
